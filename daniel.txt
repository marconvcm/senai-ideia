Problema: Estravio, dificuldade em localização, danos por quedas, danos por armazenamento inadequado em ativos de alto valor agregado.

Como resolver: Dispositivos integrados e conectados a internet, coletando informaçẽs, em tempo real, de dados de temperatura, luminosidade, ruídos e movimentação de ativos em uma empresa.

Projeto: Conjunto de sensor de ambiente (gás de cozinha ou gazes inflamáveis em geral, temperatura e luminosidade) que coleta informações e aplicativo que gerencia/mostra estas informações, conectados pela internet.





